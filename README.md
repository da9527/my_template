# 一个简单的后台管理系统模板

Vue 3 & Vite & Pinia & Vue-Router & Element-Plus & UnoCSS

一个简单的后台管理系统模板,没有什么花里胡哨的东西,还在完善中...

## 使用

安装依赖
```shell
pnpm i
```

运行项目
```shell
pnpm dev
```

访问 http://localhost:3333

打包
```shell
pnpm build
```
