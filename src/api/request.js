import axios from "axios"

const service = axios.create({
    baseURL: import.meta.env.VITE_BASE_API,
    timeout: 5000
})

// 请求拦截
service.interceptors.request.use(config => {
    // 设置token
    config.headers.Authorization = localStorage.getItem('token') || ''
    return config
}, error => {
    return Promise.reject(error)
})

// 响应拦截
service.interceptors.response.use(response => {
    const { code, data, message } = response.data
    if (code === 200) {
        return {
            data,
            message
        }
    } else {
        // 自动导包了,不用管这里的警告
        ElMessage.error(message)
        return Promise.reject(message)
    }
}, error => {
    ElMessage.error(error.message)
    return Promise.reject(error)
})

export default service