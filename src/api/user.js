import request from "./request"
import { generateSimpleUUID } from '../utils'

export const login = data => {
    // return request({
    //     url: '/login',
    //     method: 'post',
    //     data
    // })
    // 模拟登录数据
    return new Promise((resolve, reject) => {
        resolve({
            "data": {
                "id": 1,
                "username": "admin",
                "roles": "admin",
                "token": generateSimpleUUID()
            },
            "message": "登录成功"
        })
    })
}