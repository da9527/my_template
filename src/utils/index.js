export const say = () => {
    console.log('达哥好帅')
}

/**
 * 生成简单的uuid
 * @returns uuid
 */
export const generateSimpleUUID = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        const r = Math.random() * 16 | 0,
            v = c === 'x' ? r : (r & 0x3 | 0x8)
        return v.toString(16)
    })
}