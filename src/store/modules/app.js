import { defineStore } from 'pinia'
import { ref } from 'vue'

const useAppStore = defineStore('app', () => {

    // 左侧菜单栏的状态
    const siderType = ref(false)

    // 开关状态栏
    const changeSiderType = () => {
        siderType.value = !siderType.value
    }

    return {
        siderType,
        changeSiderType,
    }
}, {
    persist: true, // 持久化存储
})

export default useAppStore