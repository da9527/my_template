import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
import useUserStore from './modules/user'
import useAppStore from './modules/app'

const store = createPinia()
// 使用持久化插件
store.use(piniaPluginPersistedstate)

export default store

export {
    useAppStore,
    useUserStore
}