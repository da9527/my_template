import { createApp } from 'vue'
import router from './router'
import store from './store'
import { loadSvg } from "@/icons"
import App from './App.vue'
import 'uno.css'
import './router/permission'

const app = createApp(App)

/** 加载全局 SVG */
loadSvg(app)

app.use(router)
app.use(store)
app.mount('#app')