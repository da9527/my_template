import { createRouter, createWebHistory } from 'vue-router'
import Layout from '../layout/index.vue'
import Login from '../views/Login.vue'
import NotFound from '../views/NotFound.vue'

/**
 * 常量路由
 */
export const routes = [
    {
        path: "/",
        component: Layout,
        name: 'Layout',
        redirect: "/dashboard",
        meta: {
            // 是否在左侧菜单栏隐藏 true 隐藏 flase 不隐藏
            hidden: false
        },
        /**
         * 后面动态添加的路由都在 children 里面
         */
        children: [
            {
                path: "dashboard",
                component: () => import("@/views/Dashboard.vue"),
                name: "Dashboard",
                meta: {
                    title: "首页",
                    svgIcon: "dashboard",
                    // 是否在左侧菜单栏隐藏 true 隐藏 flase 不隐藏
                    hidden: false
                }
            },
        ],
    },
    {
        path: '/login',
        component: Login,
        name: 'Login',
        meta: {
            // 是否在左侧菜单栏隐藏 true 隐藏 flase 不隐藏
            hidden: true
        }
    },
]

/**
 * 动态路由,这是添加在 Layout 的 children 中
 */
export const dynamicRoutes = [
    {
        path: "me",
        component: () => import("@/views/me/index.vue"),
        name: "Me",
        meta: {
            title: "个人信息",
            svgIcon: "user",
            roles: ['admin', 'user'],
            // 是否在左侧菜单栏隐藏 true 隐藏 flase 不隐藏
            hidden: false
        }
    },

    {
        path: "demos",
        name: "Demos",
        meta: {
            title: "子菜单例子",
            svgIcon: "menu",
            roles: ['user', 'admin'],
            // 是否在左侧菜单栏隐藏 true 隐藏 flase 不隐藏
            hidden: false
        },
        redirect: '/demos/one',
        children: [
            {
                path: 'one',
                name: 'one',
                component: () => import('@/views/demos/one.vue'),
                meta: {
                    title: "例子1",
                    svgIcon: "link",
                    // 是否在左侧菜单栏隐藏 true 隐藏 flase 不隐藏
                    hidden: false
                }
            },
            {
                path: 'two',
                name: 'two',
                component: () => import('@/views/demos/two.vue'),
                meta: {
                    title: "例子2",
                    svgIcon: "bug",
                    // 是否在左侧菜单栏隐藏 true 隐藏 flase 不隐藏
                    hidden: false
                }
            },
        ]
    },

    /** 如果在常量路由处拦截没有配置的路由路径
        那么在没有动态添加权限路由时会直接跳到404页面
    */
    {
        path: '/404',
        component: NotFound,
        name: 'NotFound',
        meta: {
            // 是否在左侧菜单栏隐藏 true 隐藏 flase 不隐藏
            hidden: true,
            // 如果没有roles 表示谁都可以访问
        }
    }
]


/**
* 动态添加路由
* @param {string} role 角色权限
*/
export const addDynamicRoutes = (role) => {
    // 校验和生成权限路由
    for (let i = 0; i < dynamicRoutes.length; i++) {
        const item = dynamicRoutes[i]
        if (item.meta && item.meta.roles) {
            if (dynamicRoutes.some(i => i.meta.roles?.includes(role))) {
                if (!router.hasRoute(item.name)) {
                    router.addRoute('Layout', item)
                }
            }
        } else {
            // 没有权限说明是404拦截
            if (item.meta && !item.meta.roles && !router.hasRoute(item.name)) {
                router.addRoute(item)
            }
        }
    }
}


const router = createRouter({
    routes,
    history: createWebHistory()
})

export default router