import router, { addDynamicRoutes } from './index'
import { useUserStore } from '../store'
import NProgress from 'nprogress' // 引入nprogress插件
import 'nprogress/nprogress.css'  // 这个nprogress样式必须引入

// 放行的路由
const whiteList = ['/login']

// 路由前置拦截
router.beforeEach((to, from, next) => {
    NProgress.start() // 设置加载进度条(开始..)
    const userStore = useUserStore()
    const token = userStore.token
    const { roles } = userStore.userInfo
    // 如果token存在
    if ('' !== token) {
        // token存在如果还是去/login的话就直接跳转、
        if (to.path === '/login') {
            next('/')
        } else {
            // 如果路由存在就直接跳转
            if (router.getRoutes().some(i => i.path === to.path)) {
                // 添加动态路由
                addDynamicRoutes(roles)
                next()
            } else {
                // 添加动态路由
                addDynamicRoutes(roles)
                // 再判断一下路径是否真的存在
                if (router.getRoutes().some(i => i.path === to.path)) {
                    // 如果是动态添加的就需要重定向一下
                    next({ ...to, replace: true })
                } else {
                    next({ path: '/404', replace: true })
                }
            }
        }
    } else {
        if (whiteList.includes(to.path)) {
            next()
        } else {
            next('/login')
        }
    }
})

router.afterEach(() => {
    NProgress.done() // 设置加载进度条(结束..)
})