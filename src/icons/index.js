import SvgIcon from "../components/SvgIcon.vue"
import "virtual:svg-icons-register"

export function loadSvg(app) {
  app.component("SvgIcon", SvgIcon)
}
